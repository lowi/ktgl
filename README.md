# ktGL

Declarative Stateless OpenGL in Kotlin.

An early experimental prototype for doing OpenGL in Kotlin using the a similar approach as [regl](https://github.com/regl-project/regl) and [rugl](https://github.com/gregtatum/rugl).

This is an attempt at making OpenGL less infuriating in Kotlin.  
